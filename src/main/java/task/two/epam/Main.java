package task.two.epam;
/**
 * Epam task number 2
 *
 * @author Dmytro Tsurkan
 * @version 1.0 Build Nov 7, 2019
 */

public class Main {
    public static void main(String[] args) {
        WorkCore workCore = new WorkCore();
        FibonacciCalc fibonacciCalc = new FibonacciCalc();

        workCore.printUserMenuData();
        workCore.printsOddNumbers();
        workCore.printsSumOfOdd();
        workCore.printsSumOfEven();

        fibonacciCalc.printUserFibonacciData();
        fibonacciCalc.printPercentageOfOddAndEvenFibonacciNums();
    }
}
