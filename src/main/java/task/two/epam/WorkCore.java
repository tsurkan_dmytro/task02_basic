package task.two.epam;

import java.util.Scanner;
/**
 * the core class
 */

public class WorkCore {
    int firstNum = 0;
    int secondNum = 0;
    int sumOddsNums = 0;
    int sumEvenNums = 0;

    /**printsOddNumbers  */
    public void printsOddNumbers() {
        for (int i = firstNum; i < secondNum; i++){
            if (i%2 != 0){
                System.out.print(i);
                System.out.print("; ");
                sumOddsNums += i;
            }else {
                sumEvenNums += i;
            }
        }
        System.out.println();
    }

    /**printsSumOfOdd  */
    public void printsSumOfOdd() {
        System.out.println("sumOddsNums = " + sumOddsNums);
    }

    /**printsSumOfEven  */
    public void printsSumOfEven() {
        System.out.println("sumEvenNums = " + sumEvenNums);
    }

    /**printUserMenuData  */
    public void printUserMenuData() {
        System.out.println("Enter interval from 1 to 1000");
        System.out.println("Enter first number");
        Scanner in = new Scanner(System.in);
        firstNum = in.nextInt();
        System.out.println("Enter second number");
        secondNum = in.nextInt();
    }
}
