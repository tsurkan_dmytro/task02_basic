package task.two.epam;
/**
 * Class target:
 * Fibonacci calculation
 */

import java.util.Scanner;

public class FibonacciCalc {

    private int fiboNum = 0;
    private int maxOdd = 0;
    private int maxEven = 1;

    private int oddCount = 0;
    private int evenCount = 0;
    private int allCount = 0;

    /**print fibonacci numbers
     * @param end end of fibonacci array
     */
    private void printFibonacciNums(int end) {
        int n0 = 1;
        int n1 = 1;
        int n2;
        System.out.print(n0+" "+n1+" ");
        for(int i = 3; i <= end; i++){
            n2=n0+n1;
            takeMaxOdd(n2);
            takeMaxEven(n2);
            System.out.print(n2+" ");
            n0=n1;
            n1=n2;
            allCount++;
        }
        System.out.println();
        System.out.println("Max Even: " + maxEven);
        System.out.println("Max Odd: " + maxOdd);
        System.out.println();

    }

    /**printPercentageOfOddAndEvenFibonacciNums     */
    public void printPercentageOfOddAndEvenFibonacciNums() {
        System.out.println("allCount " + allCount);
        System.out.println("oddCount " + oddCount + "/ " + culcOddPerc() + "%");
        System.out.println("evenCount " + evenCount + "/ " + culcEvenPerc() + "%");

    }
    /**culculate Odd Percentage    */
    public String culcOddPerc() {
        double oddPercent = (double) oddCount / (double) allCount * 100;
        String oddResult = String.format("%.2f", oddPercent);

        return oddResult;
    }

    /**culculate Even Percentage    */
    public String culcEvenPerc() {
        double evenPercent = (double) evenCount / (double) allCount * 100;
        String evenResult = String.format("%.2f", evenPercent);

        return evenResult;
    }

    /**input abd print user fibonacci numbers     */
    public void printUserFibonacciData() {
        System.out.println("Enter Fibonacci number");
        Scanner in = new Scanner(System.in);
        fiboNum = in.nextInt();
        printFibonacciNums(fiboNum);
    }

    /**save max odd number  */
    private void takeMaxOdd(int checkNum){
        if(checkNum % 2 != 0){
                if(checkNum > maxEven)
                    maxOdd = checkNum;
                    oddCount++;
            }
    }

    /**save max even number  */
    private void takeMaxEven(int checkNum){
        if(checkNum % 2 == 0) {
            if(checkNum > maxEven)
                maxEven = checkNum;
                evenCount++;
        }
    }
}
